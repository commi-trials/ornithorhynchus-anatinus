package tk.labyrinth.oa.util;

public class MaskUtils {

	private static String maskToPattern(String mask) {
		return mask.replace("?", ".?").replace("*", ".*?");
	}

	public static boolean matches(String value, String mask) {
		return value.matches(maskToPattern(mask));
	}
}
