package tk.labyrinth.oa.financial;

import lombok.Value;
import tk.labyrinth.oa.util.MaskUtils;

import java.util.function.Supplier;

@Value
public class QuoteAggregatorSupplier {

	String mask;

	Supplier<QuoteAggregator> supplier;

	public boolean matches(String instrumentName) {
		return MaskUtils.matches(instrumentName, mask);
	}

	public QuoteAggregator supply() {
		return supplier.get();
	}
}
