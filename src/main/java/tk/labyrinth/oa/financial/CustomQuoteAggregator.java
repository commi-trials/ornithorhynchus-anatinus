package tk.labyrinth.oa.financial;

import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.NonFinal;

import java.math.BigDecimal;
import java.time.DayOfWeek;

@RequiredArgsConstructor
@Value
public class CustomQuoteAggregator extends AbstractQuoteAggregator {

	private static final BigDecimal two = new BigDecimal("2");

	@NonFinal
	int evenCount = 0;

	@NonFinal
	BigDecimal max = null;

	@NonFinal
	BigDecimal min = null;

	@NonFinal
	int mondayCount = 0;

	@NonFinal
	int oddCount = 0;

	@Override
	protected void doAggregate(Quote quote) {
		if (quote.getValue().remainder(two).compareTo(BigDecimal.ZERO) == 0) {
			evenCount++;
		}
		if (max == null || quote.getValue().compareTo(max) > 0) {
			max = quote.getValue();
		}
		if (min == null || quote.getValue().compareTo(min) < 0) {
			min = quote.getValue();
		}
		if (quote.getDate().getDayOfWeek() == DayOfWeek.MONDAY) {
			mondayCount++;
		}
		if (quote.getValue().remainder(two).compareTo(BigDecimal.ONE) == 0) {
			oddCount++;
		}
	}

	@Override
	protected String doGetData() {
		return toString();
	}
}
