package tk.labyrinth.oa.financial;

import lombok.Value;

import javax.inject.Inject;
import java.time.LocalDate;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Value
public class QuoteHub {

	ConcurrentMap<String, QuoteAggregator> aggregators = new ConcurrentHashMap<>();

	List<QuoteAggregatorSupplier> aggregatorSuppliers;

	@Inject
	public QuoteHub(List<QuoteAggregatorSupplier> aggregatorSuppliers) {
		this.aggregatorSuppliers = aggregatorSuppliers;
	}

	private boolean isWorkDay(LocalDate date) {
		return date.getDayOfWeek().getValue() >= 1 && date.getDayOfWeek().getValue() <= 5;
	}

	public void consume(Quote quote) {
		// TODO: Add test for skipping non-work days.
		if (isWorkDay(quote.getDate())) {
			aggregators.computeIfAbsent(quote.getInstrumentName(),
					key -> aggregatorSuppliers.stream()
							.filter(supplier -> supplier.matches(key))
							.findFirst()
							.map(QuoteAggregatorSupplier::supply)
							.orElseThrow()
			).aggregate(quote);
		}
	}
}
