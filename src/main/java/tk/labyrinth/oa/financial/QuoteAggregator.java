package tk.labyrinth.oa.financial;

public interface QuoteAggregator {

	/**
	 * Performed thread-safely.
	 *
	 * @param quote non-null
	 */
	void aggregate(Quote quote);

	/**
	 * @return
	 */
	String getData();
}
