package tk.labyrinth.oa.financial;

import dagger.Module;
import dagger.Provides;

import javax.inject.Inject;
import java.time.LocalDate;
import java.time.Month;
import java.util.List;

@Module
public class FinancialModule {

	@Inject
	@Provides
	public List<QuoteAggregatorSupplier> aggregatorSuppliers() {
		return List.of(
				new QuoteAggregatorSupplier("INSTRUMENT1", () -> new MeanQuoteAggregator(null, null)),
				new QuoteAggregatorSupplier("INSTRUMENT2", () -> new MeanQuoteAggregator(
						LocalDate.of(2014, Month.NOVEMBER, 1),
						LocalDate.of(2014, Month.NOVEMBER, 30))),
				new QuoteAggregatorSupplier("INSTRUMENT3", CustomQuoteAggregator::new),
				new QuoteAggregatorSupplier("*", () -> new LastNQuoteAggregator(10))
		);
	}
}
