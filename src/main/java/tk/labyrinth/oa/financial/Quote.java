package tk.labyrinth.oa.financial;

import lombok.Value;

import java.math.BigDecimal;
import java.time.LocalDate;

@Value
public class Quote {

	LocalDate date;

	String instrumentName;

	BigDecimal value;
}
