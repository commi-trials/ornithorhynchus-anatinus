package tk.labyrinth.oa.financial;

import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.NonFinal;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@RequiredArgsConstructor
@Value
public class LastNQuoteAggregator extends AbstractQuoteAggregator {

	List<Quote> lastQuotes = new ArrayList<>();

	int n;

	@NonFinal
	BigDecimal sum = BigDecimal.ZERO;

	@Override
	protected void doAggregate(Quote quote) {
		// If quote.date is before lastQuotes[0].date it is guaranteed to be discarded.
		if (lastQuotes.isEmpty() || !quote.getDate().isBefore(lastQuotes.get(0).getDate())) {
			lastQuotes.add(quote);
			if (lastQuotes.size() > n) {
				lastQuotes.sort(Comparator.comparing(Quote::getDate));
				lastQuotes.remove(0);
			}
			sum = lastQuotes.stream().map(Quote::getValue).reduce(BigDecimal.ZERO, BigDecimal::add);
		}
	}

	@Override
	protected String doGetData() {
		return toString();
	}
}
