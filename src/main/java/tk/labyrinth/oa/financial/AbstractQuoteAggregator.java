package tk.labyrinth.oa.financial;

public abstract class AbstractQuoteAggregator implements QuoteAggregator {

	private final Object lock = new Object();

	protected abstract void doAggregate(Quote quote);

	protected abstract String doGetData();

	@Override
	public void aggregate(Quote quote) {
		synchronized (lock) {
			doAggregate(quote);
		}
	}

	@Override
	public String getData() {
		synchronized (lock) {
			return doGetData();
		}
	}
}
