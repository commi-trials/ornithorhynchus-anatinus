package tk.labyrinth.oa.financial;

import lombok.Value;
import lombok.experimental.NonFinal;

import java.math.BigDecimal;
import java.math.MathContext;
import java.time.LocalDate;

@Value
public class MeanQuoteAggregator extends AbstractQuoteAggregator {

	LocalDate from;

	LocalDate to;

	@NonFinal
	int count = 0;

	@NonFinal
	BigDecimal mean = null;

	public MeanQuoteAggregator(LocalDate from, LocalDate to) {
		this.from = from;
		this.to = to;
	}

	@Override
	protected void doAggregate(Quote quote) {
		if (from == null || !from.isAfter(quote.getDate())) {
			if (to == null || !to.isBefore(quote.getDate())) {
				if (count > 0) {
					mean = mean.multiply(BigDecimal.valueOf(count))
							.add(quote.getValue())
							.divide(BigDecimal.valueOf(count + 1), MathContext.DECIMAL64);
				} else {
					mean = quote.getValue();
				}
				count++;
			}
		}
	}

	@Override
	protected String doGetData() {
		return toString();
	}
}
