package tk.labyrinth.oa;

import dagger.Component;
import tk.labyrinth.oa.financial.FinancialModule;
import tk.labyrinth.oa.financial.QuoteHub;

import javax.inject.Singleton;

@Component(modules = {FinancialModule.class})
@Singleton
public interface ApplicationComponent {

	QuoteHub quoteHub();
}
