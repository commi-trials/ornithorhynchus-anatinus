package tk.labyrinth.oa.financial;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;

class CustomQuoteAggregatorTest {

	@Test
	void testAggregate() {
		LocalDate date = LocalDate.of(2019, 10, 27);
		QuoteAggregator aggregator = new CustomQuoteAggregator();
		Assertions.assertEquals("CustomQuoteAggregator(evenCount=0, max=null, " +
				"min=null, mondayCount=0, oddCount=0)", aggregator.getData());
		//
		aggregator.aggregate(new Quote(date.minusDays(2), null, new BigDecimal("1")));
		aggregator.aggregate(new Quote(date.minusDays(1), null, new BigDecimal("2")));
		aggregator.aggregate(new Quote(date.plusDays(1), null, new BigDecimal("3")));
		aggregator.aggregate(new Quote(date.plusDays(2), null, new BigDecimal("0.25")));
		Assertions.assertEquals("CustomQuoteAggregator(evenCount=1, max=3, " +
				"min=0.25, mondayCount=1, oddCount=2)", aggregator.getData());
	}
}
