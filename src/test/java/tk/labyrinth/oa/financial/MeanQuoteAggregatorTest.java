package tk.labyrinth.oa.financial;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;

class MeanQuoteAggregatorTest {

	@Test
	void testAggregate() {
		LocalDate date = LocalDate.of(2019, 10, 27);
		QuoteAggregator aggregator = new MeanQuoteAggregator(date.minusDays(1), null);
		Assertions.assertEquals("MeanQuoteAggregator(from=2019-10-26, to=null, " +
				"count=0, mean=null)", aggregator.getData());
		//
		aggregator.aggregate(new Quote(date.minusDays(2), null, new BigDecimal("1")));
		aggregator.aggregate(new Quote(date.minusDays(1), null, new BigDecimal("2")));
		aggregator.aggregate(new Quote(date.plusDays(1), null, new BigDecimal("3")));
		aggregator.aggregate(new Quote(date.plusDays(2), null, new BigDecimal("3")));
		Assertions.assertEquals("MeanQuoteAggregator(from=2019-10-26, to=null, " +
				"count=3, mean=2.666666666666667)", aggregator.getData());
	}
}
