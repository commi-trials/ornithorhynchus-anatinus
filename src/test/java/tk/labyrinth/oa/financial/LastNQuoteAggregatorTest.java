package tk.labyrinth.oa.financial;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;

class LastNQuoteAggregatorTest {

	@Test
	void testAggregate() {
		LocalDate date = LocalDate.of(2019, 10, 27);
		QuoteAggregator aggregator = new LastNQuoteAggregator(2);
		Assertions.assertEquals("LastNQuoteAggregator(lastQuotes=[" +
				"], n=2, sum=0)", aggregator.getData());
		//
		aggregator.aggregate(new Quote(date, null, new BigDecimal("1")));
		aggregator.aggregate(new Quote(date.plusDays(1), null, new BigDecimal("2")));
		aggregator.aggregate(new Quote(date.minusDays(2), null, new BigDecimal("3")));
		Assertions.assertEquals("LastNQuoteAggregator(lastQuotes=[" +
				"Quote(date=2019-10-27, instrumentName=null, value=1), " +
				"Quote(date=2019-10-28, instrumentName=null, value=2)" +
				"], n=2, sum=3)", aggregator.getData());
		//
		aggregator.aggregate(new Quote(date.plusDays(1), null, new BigDecimal("4")));
		Assertions.assertEquals("LastNQuoteAggregator(lastQuotes=[" +
				"Quote(date=2019-10-28, instrumentName=null, value=2), " +
				"Quote(date=2019-10-28, instrumentName=null, value=4)" +
				"], n=2, sum=6)", aggregator.getData());
	}
}
