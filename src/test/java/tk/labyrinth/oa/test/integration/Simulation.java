package tk.labyrinth.oa.test.integration;

import org.junit.jupiter.api.Test;
import tk.labyrinth.oa.DaggerApplicationComponent;
import tk.labyrinth.oa.financial.Quote;
import tk.labyrinth.oa.financial.QuoteHub;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class Simulation {

	private static final DateTimeFormatter localDateReadFormatter = DateTimeFormatter.ofPattern(
			"dd-MMM-yyyy", Locale.ENGLISH);

	@Test
	void simple() throws IOException {
		QuoteHub quoteHub = DaggerApplicationComponent.create().quoteHub();
		Files.newBufferedReader(Paths.get("task/Financial_instruments_input.txt")).lines()
				.forEach(line -> {
					String[] splittedLine = line.split(",");
					quoteHub.consume(new Quote(
							LocalDate.parse(splittedLine[1], localDateReadFormatter),
							splittedLine[0],
							new BigDecimal(splittedLine[2])
					));
				});
		quoteHub.getAggregators().entrySet().forEach(System.out::println);
	}
}
